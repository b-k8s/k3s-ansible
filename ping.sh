#!/bin/bash
for h in worker1 worker2 worker3 worker4 worker5 ctrl1 mgmt1
do
  ansible -i inventory/beast/hosts.ini -m ping $h
done
