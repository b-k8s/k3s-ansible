#!/bin/bash
usage() {
  echo "Error. You need to pass a hostname."
  echo "  ./apply-single.sh hostname"
}

if [ $# -eq 0 ]
  then
    usage
  else
    ansible-playbook site.yml \
      -i inventory/beast/hosts.ini \
      --vault-password-file=pass.txt \
      -l $1
fi
