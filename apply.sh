#!/bin/bash
ansible-playbook site.yml \
  -i inventory/beast/hosts.ini \
  --vault-password-file=pass.txt
