# Build a Kubernetes cluster using k3s via Ansible

## Install process

Run `./apply.sh`

## Upgrade process

This does a rolling update of k3s.

### Step 1 - Set the version you want

Edit `ks3_version` in `inventory/beast/group_vars/all.yml` and set your version.

### Step 2 - Upgrade ctrl and mgmt nodes together

Run `./apply-single.sh ctrl1,mgmt1`

### Step 3 - Upgrade the worker nodes, one by one

Run
```
for w in worker{1..7}; do echo ./apply-single.sh $w; done | bash -xe
```

### Step 4 - Copy the new kubeconfig file into place

From your dev workstation run `./get_kube_config.sh`
